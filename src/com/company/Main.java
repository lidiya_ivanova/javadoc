package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner comScanner = new   Scanner(new File("C:\\File.java"));
        // для хранения таблицы
        String[] str_tabl_1  = new String[5];
        // обнуление переменных
        for (int i = 0; i < str_tabl_1.length;i++){
            if (i == 1){
                str_tabl_1[i] = "";
            } else{
                str_tabl_1[i] = "Null";
            }
        }

        // создание первой таблицы
        while (comScanner.hasNextLine()) {
            String str = comScanner.nextLine();

            // обработка комментария
            if (str.toLowerCase().contains("@author")){
                // удаляем из строки символ комментария, тег автора и боковые пробелы
                str_tabl_1[2] = str.replace("*", "").replace("@author", "").trim();
            }
            else if (str.toLowerCase().contains("@version")){
                str_tabl_1[3] = str.toLowerCase().replace("*", "").replace("@version", "").trim();
            }
            else if (str.toLowerCase().contains("@since")) {
                str_tabl_1[4] = str.toLowerCase().replace("*", "").replace("@since", "").trim();
            }
            else if (str.toLowerCase().contains("/**") || str.toLowerCase().contains("*")){ // начало комента или без тега
                str_tabl_1[1] += str.toLowerCase().replace("/", "").replace("*", "").trim() + " ";
            }

            if (str.toLowerCase().contains("class")){
                str_tabl_1[0] = str.toLowerCase().replace("{", "").trim();
                break;
            }
        }


        // переменная таблицы
        ArrayList<String[]> tabl_2 = new ArrayList<String[]>();
        // одна строка таблицы
        String[] str_tabl_2  = new String[4];
        // обнуление переменных
        for (int i = 0; i < str_tabl_2.length;i++){
            if (i == 1){
                str_tabl_2[i] = "";
            } else{
                str_tabl_2[i] = "Null";
            }
        }
        // создание второй таблицы
        while (comScanner.hasNextLine()) {
            String str = comScanner.nextLine();
            // обработка комментария
            if (str.toLowerCase().contains("@return")){
                // удаляем из строки символ комментария, тег и боковые пробелы
                str_tabl_2[3] = str.toLowerCase().replace("*", "").replace("@return", "").trim();
            }
            else if (str.toLowerCase().contains("@param")){
                // Если переменная была пустой, то удаляем пустоту
                if (str_tabl_2[2] == "Null"){
                    str_tabl_2[2] = "";
                }
                str_tabl_2[2] += str.toLowerCase().replace("*", "").replace("@param", "").trim() + "<br>";
            }
            else if (str.toLowerCase().contains("/**") || str.toLowerCase().contains("*")){ // начало комментария или без тега
                str_tabl_2[1] += str.toLowerCase().replace("/", "").replace("*", "").trim() +  " ";
            }

            if (str.toLowerCase().contains("*/")){ // если нашли символ последнего комментария
                while (comScanner.hasNextLine()) { // идем дальше по строкам
                    String sign_not = comScanner.nextLine();
                    if(!sign_not.trim().equals("")){ // пока не находим пустую строку
                        //строка после комментария
                        str_tabl_2[0] = sign_not.replace("{", "").replace(";", "").trim();
                        //добавляем в массив таблицу
                        tabl_2.add(str_tabl_2.clone());
                        // обновляем переменные
                        for (int i = 0; i < str_tabl_2.length;i++){
                            if (i == 1){
                                str_tabl_2[i] = "";
                            } else{
                                str_tabl_2[i] = "Null";
                            }
                        }
                        break;
                    }
                }
            }
        }

        String report = generateReport(str_tabl_1, tabl_2);
        saveToFile(report, "C:\\report.html");
    }
    // создание html
    private static String generateReport(String[] str, ArrayList<String[]> list) {
        StringBuilder stringBuilder = new StringBuilder()
                .append("<!DOCTYPE html>\n")
                .append("<html lang=\"en\">\n")
                .append("<head>\n")
                .append("    <meta charset=\"UTF-8\">\n")
                .append("    <link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\" title=\"Style\">\n")
                .append("    <title>Иванова</title>\n")
                .append("<style>\n")
                .append("        body {\n")
                .append("            background-color: #fff;\n")
                .append("            color: #353833;\n")
                .append("            font-family: Arial, sains-serif;\n")
                .append("            font-size: 14px;\n")
                .append("        }\n")
                .append("        table {\n")
                .append("            width: 100%;\n")
                .append("            margin-bottom: 20px")
                .append("            border: 1px solid #dddddd;\n")
                .append("            border-collapse: collapse;\n")
                .append("        }\n")
                .append("        TD {\n")
                .append("            border: 1px solid #dddddd;\n")
                .append("            padding: 5px;\n")
                .append("        }\n")
                .append("        TH{\n")
                .append("            font-weight: bold;\n")
                .append("             padding: 5px;")
                .append("           background: #efefef;\n")
                .append("            border: 1px solid #dddddd;\n")
                .append("        }\n")
                .append("\n")
                .append("    </style>")
                .append("</head>\n")
                .append("<body>\n")
                .append("    <h1>Отчет</h1>\n");

        //шапка таблицы 1
        stringBuilder
                .append("<table>\n")
                .append("<tr bgcolor=\"#D3D3D3\">\n")
                .append("<td>Класс</td>\n")
                .append("<td>Описание</td>\n")
                .append("<td>Автор</td>\n")
                .append("<td>Версия</td>\n")
                .append("<td>Since версия</td>\n")
                .append("</tr>\n")
                .append("<tr>\n");
        // формируется таблица 1
        for (int i = 0; i < str.length; i++) {
            stringBuilder.append("<td>")
                    .append(str[i])
                    .append("</td>\n");
        }
        // конец таблицы
        stringBuilder
                .append("</tr>\n")
                .append("</table>");


        stringBuilder.append("<p><br><p>"); // пропуск между таблицами

        //шапка таблицы
        stringBuilder
                .append("<table>\n")
                .append("<tr bgcolor=\"#D3D3D3\">\n")
                .append("<td>Подпись</td>\n")
                .append("<td>Описание</td>\n")
                .append("<td>Параметры</td>\n")
                .append("<td>Вывод</td>\n")
                .append("</tr>\n");
        // создание второй таблицы
        for (String[] str_mass : list){
            stringBuilder.append("<tr>");
            for (int i = 0; i < str_mass.length; i++)
                stringBuilder.append("<td>")
                        .append(str_mass[i])
                        .append("</td>\n");
            stringBuilder.append("</tr>");
        }
        //конец второй таблицы
        stringBuilder
                .append("</table>\n");

        // конец сайта
        stringBuilder
                .append("</body>\n")
                .append("</html>");

        return stringBuilder.toString();
    }
// сохранение html
    private static void saveToFile(String report, String fileName) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName));
        writer.write(report);
        writer.close();
    }
}


