package com.company;

import java.util.ArrayList;

/**
 * @author Лида Иванова
 * @version 1.1
 * @since 12.5
 * Класс для хранения габаритных товаров
 */
public class DimensionalGoods extends Commodity{
    /**
     * Переменная для хранения высоты
     */
    private  int height;
    /**
     * Переменная для хранения ширины
     */
    private  int width;
    /**
     * Переменная для хранения длинны
     */
    private  int length;

    /**
     * Конструктор для переменных
     * @param id --  наследуемый параметр id товара
     * @param productCode --  наследуемый параметр кода товара
     * @param price --  наследуемый параметр цены товара
     * @param name --  наследуемый параметр имени товара
     * @param description --  наследуемый параметр описания товара
     * @param height
     * @param width
     * @param length
     */
    public DimensionalGoods(int id, int productCode, double price, String name, String description, int height, int width, int length) {
        super(id, productCode, price, name, description);
        this.height = height;
        this.width = width;
        this.length = length;
    }

    /**
     * Метод для вывода информации на экран
     * @return текст выводящий параметры товара
     */
    @Override
    public String toString() {
        return    super.toString() + "\n" +
                 "height=" + height + "\n" +
                 "width=" + width + "\n" +
                 "length=" + length + "\n" ;
    }
}
